#!/bin/bash

# Upload an Arduino hexfile directly to a target device.
# Mithat Konar 2017-2018

PATH_TO_ARDUINO=/home/mithat/opt/arduino
DEVICE=/dev/ttyUSB0
HEXFILE=build-uno/arduino-hex-burn.hex

$PATH_TO_ARDUINO/hardware/tools/avr/bin/avrdude -D -V -p atmega328p -C $PATH_TO_ARDUINO/hardware/tools/avr/etc/avrdude.conf -c arduino -b 115200 -P $DEVICE -U flash:w:$HEXFILE:i
