arduino-hex-burn
===============

Upload an Arduino hexfile directly to a target device.

Mithat Konar 2017-2018

License
-------
[GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html)

How to use
----------
This was developed for use with Linux computers and a USB-to-serial programmer.

### Configuration

Download this repository someplace convenient relative to the hexfile. Open `arduino-hex-burn.sh` in a text editor change the configuration parameters:

```
PATH_TO_ARDUINO=/home/username/place/you/installed/arduino
DEVICE=/dev/<the-programmer-device>
HEXFILE=path/to/the/hexfile.hex
```

`PATH_TO_ARDUINO` is where the main arduino package is installed. This is **not** the directory where you store your Arduino projects (typically `/home/username/Arduino`). Don't use a trailing slash. There can't be any spaces in the path.

You can get `<the-programmer-device>` in two ways:
1. With the programmer plugged in, open the Arduino IDE and look under _Tools > Port_. The device listed there is probably the programmer.
2. In a terminal, try:
    ```
    ls /dev/ttyUSB*
        and
    ls /dev/ttyACM*
    ```
    
    One of those is almost certainly the programmer. You'll have to change the value of the DEVICE configuration parameter until you find it.

Note that the device value of the programmer device can change after you unplug it.

### Do it

Burn the hexfile to the microcontroller by opening a terminal in the directory where this script is and enter:

```
./arduino-hex-burn.sh
```

The dot and slash in front of the script name are mandatory.

Problems and solutions
----------------------

#### `bash: ./arduino-hex-burn.sh: Permission denied`

You need to set `arduino-hex-burn.sh` to be executable. From the terminal, issue the following command:

```
chmod +x arduino-hex-burn.sh
```

Or you can use your file manager to set the file to be executable if you prefer.

#### `<something>/hardware/tools/avr/bin/avrdude: No such file or directory`

The PATH_TO_ARDUINO configuration parameter is borked. Make sure it's pointing to the right place.

#### `avrdude: ser_open(): can't open device "/dev/<whatever>": No such file or directory`

The DEVICE configuration parameter is borked. Make sure it's pointing to the right device.
